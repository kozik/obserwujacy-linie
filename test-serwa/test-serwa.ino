#include <Servo.h>

Servo s;

void setup() {
  Serial.begin(9600);
  Serial.println("Obserwujacy linie!");
  s.attach(9);
}

void loop() {
  Serial.println("0");
  s.write(0);
  delay(5000);

  Serial.println("90");
  s.write(90);
  delay(5000);

  Serial.println("180");
  s.write(180);
  delay(5000);
}
