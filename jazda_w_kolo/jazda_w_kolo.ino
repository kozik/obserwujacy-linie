#include <Servo.h>

int STOP = 90;

Servo sl, sr;

void setup() {
  Serial.begin(9600);
  Serial.println("Obserwujacy linie!");
  sr.attach(9);
  sl.attach(10);

  sr.write(STOP);
  sl.write(STOP);
}

void loop() {
  int speed = 10;
  sr.write(STOP);
  sl.write(STOP);
  delay(2000);

  sr.write(STOP + speed);
  sl.write(STOP + speed);
  delay(2000);


  sr.write(STOP - speed);
  sl.write(STOP - speed);
  delay(2000);
}
