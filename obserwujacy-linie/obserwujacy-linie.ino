#include <Servo.h>

int L1 = 2;
int L2 = 3;
int L3 = 4;
int L4 = 5;
int L5 = 6;

int STOP = 90;

Servo sl, sr;

void setup() {
  Serial.begin(9600);
  Serial.println("Obserwujacy linie!");
  sr.attach(9);
  sl.attach(10);

  sr.write(STOP);
  sl.write(STOP);

  pinMode(L1, INPUT);
  pinMode(L2, INPUT);
  pinMode(L3, INPUT);
  pinMode(L4, INPUT);
  pinMode(L5, INPUT);
}

void loop() {
  int speed = 10;
  int l1, l2, l3, l4, l5;

  l1 = digitalRead(L1);
  l2 = digitalRead(L2);
  l3 = digitalRead(L3);
  l4 = digitalRead(L4);
  l5 = digitalRead(L5);

  if (l3 == 0) {
      sr.write(STOP - speed);
      sl.write(STOP + speed);
  } else if (l2 == 0) {
      sr.write(STOP - speed);
      sl.write(STOP - speed);    
  } else if (l4 == 0) {
      sr.write(STOP + speed);
      sl.write(STOP + speed);    
  }

  delay(100);
}
