int L1 = 2;
int L2 = 3;
int L3 = 4;
int L4 = 5;
int L5 = 6;

void setup() {
  pinMode(L1, INPUT);
  pinMode(L2, INPUT);
  pinMode(L3, INPUT);
  pinMode(L4, INPUT);
  pinMode(L5, INPUT);
  Serial.begin(9600);
  Serial.println("Test czujnika linii");
}

void loop() {
  int l1, l2, l3, l4, l5;

  l1 = digitalRead(L1);
  l2 = digitalRead(L2);
  l3 = digitalRead(L3);
  l4 = digitalRead(L4);
  l5 = digitalRead(L5);

  Serial.print(l1);
  Serial.print(l2);
  Serial.print(l3);
  Serial.print(l4);
  Serial.print(l5);
  Serial.println("");
  delay(200);
}
