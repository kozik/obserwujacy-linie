# Robot obserwujący linie

Czy chciałbyś zbudować własnego robota? Jeżeli tak dzisiaj właśnie dowiemy się jak.
Krok po kroku złożymy i zaprogramujemy robota podążającego za linią.
Kolejne etapy są opisane poniżej oraz pokazane w filmie:

https://youtu.be/jU4nPJ80r-c

## Materiały

### Części

![Niezbędne części](README_IMG/parts_num.PNG)

1. Arduino UNO (może być klon),
2. Serwomechanizm MG90S **360** stopni dwie sztuki,
3. Moduł śledzenia linii z pięcioma czujnikami TCRT5000L,
4. Koszyk na baterie 3xAA R6 z pokrywką i włącznikiem,
5. Przewody połączeniowe z wtykami i gniazdami goldpin,
6. Karton o wymiarach 14 cm na 11 cm,
7. Dwie nakrętki, które posłużą jako koła,
8. Nakrętka z wody utlenionej, która posłuży jako przednia podpórka (może być cokolwiek innego).

### Narzędzia

![NARZĘDZIA](README_IMG/tools.png)

1. Kabel USB-A (jak do drukarki),
2. klej na gorąco,
3. nożyczki,
4. obcęgi do cięcia drucików i ściągania izolacji,
5. taśma izolacyjna.

### Oprogramowanie

Aby otworzyć i skompilować, a następnie zaprogramować płytkę Arduino
potrzebne jest Arduino IDE.
Można je pobrać (za darmo) ze strony [arduino.cc](https://www.arduino.cc/en/software).

## Czujnik linii

Jak wskazuje nazwa pozwala on wykryć linię.
Składa się z diody świecącej oraz czujnika światła.

![Czujnik linii](README_IMG/line_sensor.png)

Jak zapewne wiemy, jasna powierzchnia odbija światło z powrotem
do czujnika, natomiast czarna linia pochłonie większość padających na nią fal.
Na płytce znajduje się także układ elektroniczny, który zmienia odczytany
analogowy sygnał na cyfrową informację:

 - 1 logiczne (5V) - nie wykryto linii,
 - 0 logiczne (0V) - wykryto linię.

Gdy połączymy wyjście czujnika z wejściem cyfrowym Arduino,
będziemy mogli w programie odczytywać jego stan i używać go
do podejmowania decyzji.

Dodatkowo na płytce nad każdym czujnikiem znajduje się dioda świecąca.
Pokazuje ona informację zwracaną przez sensor. Możemy podłączyć koszyk
z bateriami do złącza i przykłądając moduł do białej 
kartki z naklejoną taśmą sprawdzić jak się będzie zachowywał.

Wyjścia czujnika możemy podłączyć do dowolnych wejść Arduino.
Ponieważ chcemy też mieć możliwość używania portu szeregowego pomijamy piny 0 i 1.
Dlatego podłączamy je do wejść od 2 do 6.
Aby sprawdzić działanie wybieramy szkic test-czujnika-linii.

Najpierw funkcji `setup` konfigurujemy odpowiednie piny jako
wejścia cyfrowe:

```
pinMode(L1, INPUT);
```

Gdzie `L1` jest stałą, do której wpiszemy numer pinu:

```
int L1 = 2;
```

Następnie możemy odczytać jego stan za pomocą funkcji:

```
int l1;

l1 = digitalRead(L1);
```

Po zaprogramowaniu Arduino otwieramy monitor portu szeregowego,
który będzie pokazywał kolejne odczyty. Możemy sprawdzić działanie
przykładając czujnik do białego papieru z naklejonym fragmentem
czarnej taśmy.

![Czujnik linii](README_IMG/line_sensor_test.png)

## Jak działa serwo 360
Serwomechanizm modelarski skłąda się z silnika elektrycznego,
przekładni oraz sterownika. Występują w dwóch wersjach:
 - zadawanie pozycji (kąta),
 - zadawanie prędkości (tak zwane servo 360).
My będziemy używali tego drugiego.
   
Serwomechanizm ma trzy przewody:
 - czarny to masa (minus) zasilania,
 - czerwony to plus zasilania, zwykle 5V,
 - żółty (najczęściej) to sygnał sterujący, który podłączamy do Arduino.

Sygnał sterujący musi być podłączony do tak zwanych pinów PWM.
Są one oznaczone na płytce znakiem ~ (tylda).

Aby zobaczyć jak działa serwo użyjemy szkicu (czyli programu dla Arduino)
test-serwa. Na samym początku dołączamy bibliotekę obsługującą
serwomechanizm:
```
#include <Servo.h>
```

Oraz deklarujemy zmienną globalną reprezentującą serwo:

```
Servo s;
```

Następnie w funkcji `setup()` ustawiamy numer pinu Arduino do którego
jest podłączony serwomechanizm:

```
s.attach(9);
```

W naszym przypadku jest to pin numer 9.

Prędkość zadajemy za pomocą funkcji:

```
s.write(90);
```

Gdzie podana liczba oznacza prędkość. Jak widac na poniższym rysunku:
 - 90 to stop,
 - 0 do 89 to prędkość obrotu zgodnie z ruchem wskazówek zegara,
 - 91-180 to prędkość obrotu przeciwnie do ruchu wskazówek zegara.

![Kierunek obrotu serwomechanizmu](README_IMG/servo.png)

## Montujemy robota

Cała budowa została pokazana na filmie, ale tu także umówimy najważniejsze kroki.
Najpierw do nakrętek, które pełnią funkcję kół, przyklejamy orczyki serw.

![Montaż kół](README_IMG/m_1.png)

Podstawą robota stanowi prostokątny fragment kartonu o wymiarach
11 na 14 cm. Przyklejamy do niej koszyk na baterie (wyłączamy przełącznik) oraz Arduino.

![Montaż kół](README_IMG/m_2.png)

Następnie przyklejamy serwomechanizmy:

![Montaż kół](README_IMG/m_3.png)

Teraz odwracamy karton i przyklejamy nakrętkę
z wody utlenionej jako przednią podpórkę:

![Montaż kół](README_IMG/m_4.png)

Kolejny krok do przyklejenie czujnika linii:

![Montaż kół](README_IMG/m_5.png)

Jeśli będzie on opadać, można dokleić go taśmą izolacyjną.
Na końcu montujemy koła i nasz pojazd jest gotowy.

## Łączymy druciki
Sposób połączenia elektroniki przedstawia poniższy schemat.

![Schemat montażowy](README_IMG/all.png)

Przed rozpoczęciem montażu musimy pamiętać o wyłączeniu przełącznika na koszyku z bateriami.

Wyjścia czujników podłączamy do wejść D2 - D6 na płytce Arduino. Sygnały sterujące serwomechanizmów
podłączamy do wejść D9 (prawy) i D10 (lewy).
Na końcu łączymy zasilanie: masę oraz 5V. Przecinamy przewody połączeniowe na pół i ściągamy izolację z końcówki.
Wtyczkę wkładamy do odpowiednich przyłączy, a wolne końce splątujemy ze sobą. Na końcu zabezpieczamy je taśmą izolacyjną.

## Jazda
Na początku sprawdzimy działanie silników.
Posłuży nam szkic jazda_w_kolo.ino.
W części `setup()` konfigurujemy serwomechanizmy.
Następnie w `loop()` po kolei zatrzymujemy, a następnie wykonuje obroty w jedną, a następnie w drugą stronę.

## Obserwujemy linię
Program sterujący robotem znajdziemy w obserwujacy_linie.ino.
Na początku następuje konfiguracja serw i czujników.
W pętli głównej odczytujemy stan czujników linii.
Algorytm sterowania jest prosty. Jeżeli środkowy czujnik wykryje
linię, robot będzie jechał prosto. W przeciwnym razie, gdy
lewy albo prawy czujnik złapie linie, nastąpi skręt w odpowiednią stronę.
Natomiast gdy żaden z czujników nie jest aktywny, robot będzie kontynuował poprzednią akcję.

Teraz pozostało nam już tylko przygotować tor i sprawdzić działanie robota.

## Co dalej
Zachęcam do własnych modyfikacji robota. Zarówno jego części mechanicznej,
jak i programu. Czy masz pomsł jak zmienić program, aby robot szybciej pokonywał zadaną trasę?